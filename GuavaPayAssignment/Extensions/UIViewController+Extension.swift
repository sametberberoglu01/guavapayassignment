//
//  UIViewController+Extension.swift
//  GuavaPayAssignment
//
//  Created by Samet Berberoğlu on 21.05.2022.
//

import UIKit

@objc extension UIViewController {
    
    class var storyboardName: String { "" }
    
    class func sb_instantiateInstance() -> Self {
        return instantiateInstance(storyboardName: storyboardName, storyboardId: className)
    }
    
}

extension UIViewController {
    
    func presentWarningAlertController(title: String? = "Warning", message: String?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .cancel)
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
    }
    
    private class func instantiateInstance<T>(storyboardName: String, storyboardId: String) -> T {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: storyboardId) as! T
    }
    
}
