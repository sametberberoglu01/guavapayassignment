//
//  UIColor+Extension.swift
//  GuavaPayAssignment
//
//  Created by Samet Berberoğlu on 21.05.2022.
//

import UIKit

extension UIColor {
    
    enum Custom: String, CaseIterable {
        /// Light(r:51 g:51 b:51) Dark(r:255  g:255  b:255)
        case bodyActiveText
       
        /// Light(r:153 g:153 b:153) Dark(r:144 g:144 b:144)
        case bodyPassiveText
        
        /// Light(r:242 g:242 b:242) Dark(r:25 g:25 b:25)
        case bodyBackground
     
        /// Light(r:255 g:255 b:255) Dark(r:49 g:49 b:49)
        case cellBackground
        
        /// Light(r:249 g:249 b:249) Dark(r:51 g:51 b:51)
        case navbarBackground
  
        /// Light(r:215 g:25 b:32) Dark(r:255 g:255 b:255)
        case navbarTint
        
    }
    
    class func customDynamicColor(_ customColor: Custom) -> UIColor {
        return UIColor(named: customColor.rawValue)!
    }
    
}

