//
//  UITableView+Extension.swift
//  GuavaPayAssignment
//
//  Created by Samet Berberoğlu on 22.05.2022.
//

import UIKit

extension UITableView {
    
    func sb_registerCell(_ cellClass: UITableViewCell.Type) {
        register(UINib(nibName: cellClass.className,
                       bundle: nil),
                 forCellReuseIdentifier: String(describing: cellClass.self))
    }
    
    func sb_dequeueCell<T: UITableViewCell>(for indexPath: IndexPath) -> T {
        let cell = dequeueReusableCell(withIdentifier: T.className,
                                       for: indexPath) as! T
        return cell
    }
    
}
