//
//  NSObject+Extension.swift
//  GuavaPayAssignment
//
//  Created by Samet Berberoğlu on 21.05.2022.
//

import Foundation

extension NSObject {
    
    class var className: String {
        let name = description()
        
        if name.contains(".") {
            if let classNameWithoutModuleName = name.components(separatedBy: ".").last {
                return classNameWithoutModuleName
            }
            return ""
        }
        return name
    }
    
    var className: String {
        let name = String(describing: type(of: self))
        
        if name.contains(".") {
            if let classNameWithoutModuleName = name.components(separatedBy: ".").last {
                return classNameWithoutModuleName
            }
            return ""
        }
        return name
    }
    
}
