//
//  SBNavigationController.swift
//  GuavaPayAssignment
//
//  Created by Samet Berberoğlu on 21.05.2022.
//


import UIKit

final class SBNavigationController: UINavigationController {
    
    override init(rootViewController: UIViewController) {
        super.init(rootViewController: rootViewController)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        view.backgroundColor = .customDynamicColor(.bodyBackground)
        navigationBar.tintColor = .customDynamicColor(.navbarTint)
        navigationBar.barTintColor = .customDynamicColor(.navbarTint)
        
        let titleTextAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor.customDynamicColor(.navbarTint),
                                                                  .font: UIFont.systemFont(ofSize: 17.0, weight: .semibold)]
        
        let largeTitleTextAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor.customDynamicColor(.navbarTint),
                                                                  .font: UIFont.systemFont(ofSize: 32.0, weight: .bold)]
        let appearance = UINavigationBarAppearance()
        appearance.configureWithOpaqueBackground()
        appearance.backgroundColor = .customDynamicColor(.navbarBackground)
        appearance.titleTextAttributes = titleTextAttributes
        appearance.largeTitleTextAttributes = largeTitleTextAttributes
        appearance.shadowColor = .clear
        
        UINavigationBar.appearance().standardAppearance = appearance
        UINavigationBar.appearance().scrollEdgeAppearance = appearance
        UINavigationBar.appearance().compactAppearance = appearance
        if #available(iOS 15.0, *) {
            UINavigationBar.appearance().compactScrollEdgeAppearance = appearance
        }
        navigationBar.prefersLargeTitles = true
    }
    
}
