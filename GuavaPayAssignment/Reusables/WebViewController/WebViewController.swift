//
//  WebViewController.swift
//  GuavaPayAssignment
//
//  Created by Samet Berberoğlu on 27.05.2022.
//

import UIKit
import WebKit

class WebViewController: BaseViewController {

    @IBOutlet private weak var webView: WKWebView!
    
    private var baseUrlString: String?
    private var navigaitonTitle: String?
    
    class func present(baseUrlString: String?, navigaitonTitle: String?, from viewController: BaseViewController) {
        let vc = WebViewController(baseUrlString: baseUrlString, navigaitonTitle: navigaitonTitle)
        let navC = SBNavigationController(rootViewController: vc)
        navC.modalPresentationStyle = .fullScreen
        viewController.present(navC, animated: true, completion: nil)
    }
    
    init(baseUrlString: String?, navigaitonTitle: String?) {
        self.baseUrlString = baseUrlString
        self.navigaitonTitle = navigaitonTitle
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    private func setupUI() {
        navigationItem.title = navigaitonTitle
        webView.navigationDelegate = self
        webView.backgroundColor = .clear
        
        if let baseUrlString = baseUrlString, let absoluteUrl = URL(string: baseUrlString) {
            webView.load(URLRequest(url: absoluteUrl))
        }
    }

}

extension WebViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        decisionHandler(.allow)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        requestInProgress = false
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        requestInProgress = false
    }
    
}

