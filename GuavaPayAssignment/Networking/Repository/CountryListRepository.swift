//
//  CountryListRepository.swift
//  GuavaPayAssignment
//
//  Created by Samet Berberoğlu on 23.05.2022.
//

import Foundation

protocol CountryListRepositoryProtocol {
    func fetchCountryList(for continent: Continent)
    func fetchAllCountries()
    func fetchCountryDetail(for code: String)
}

class CountryListRepository: CountryListRepositoryProtocol {
    
    var client: NetworkProviderProtocol?
    @Published var countryList = [Country]()
    @Published var error: Error?
    
    private var previousDataTask: URLSessionDataTask?
    
    init(client: NetworkProviderProtocol? = nil) {
        self.client = client
    }
    
    func fetchCountryList(for continent: Continent) {
        previousDataTask?.cancel()
        client = CountryListServiceClient(urlSession: client?.urlSession ?? Session(),
                                          clientService: client?.service ?? CountryListService(endpoint: .continent(continent: continent)))
        fetchCountryList(client: client!)
    }
    
    func fetchAllCountries() {
        previousDataTask?.cancel()
        client = CountryListServiceClient(urlSession: client?.urlSession ?? Session(),
                                          clientService: client?.service ?? CountryListService(endpoint: .allCountries))
        fetchCountryList(client: client!)
    }
    
    func fetchCountryDetail(for code: String) {
        previousDataTask?.cancel()
        client = CountryListServiceClient(urlSession: client?.urlSession ?? Session(),
                                          clientService: client?.service ?? CountryListService(endpoint: .countryDetail(code: code)))
        fetchCountryList(client: client!)
    }
    
}

private extension CountryListRepository {
    
    func fetchCountryList(client: NetworkProviderProtocol) {
        countryList = []
        previousDataTask = client.dataTask(dataType: [Country].self, onQueue: .main) { [weak self] result in
            do {
                self?.countryList = try result.get()
            } catch {
                self?.error = error
            }
        }
        previousDataTask?.resume()
    }
    
}
