//
//  Country.swift
//  GuavaPayAssignment
//
//  Created by Samet Berberoğlu on 23.05.2022.
//

import Foundation

class Country: Decodable, Hashable {
    
    var name: CountryName?
    var flag: String?
    var borders: [String]?
    let cca3: String
    private var currencyDictionary: [String: CountryCurrency]?
    private var languageDictionary: [String: String]?
    private var flagDictionary: [String: String]?
    private var mapDictionary: [String: String]?
    
    lazy var currencies: [CountryCurrency] = {
        return currencyDictionary?.compactMap({ (currencyCode, countryCurrency) in
            countryCurrency.currencyCode = currencyCode
            return countryCurrency
        }) ?? []
    }()
    
    lazy var languages: [CountryLanguage] = {
        return languageDictionary?.compactMap({ CountryLanguage(languageCode: $0, language: $1) }) ?? []
    }()
    
    lazy var flagUrlString: String? = {
        return flagDictionary?["png"]
    }()
    
    lazy var googleMapsUrlString: String? = {
        return mapDictionary?["googleMaps"]
    }()
    
    private enum CodingKeys: String, CodingKey {
        case name, flag, cca3, borders
        case currencyDictionary = "currencies"
        case languageDictionary = "languages"
        case flagDictionary = "flags"
        case mapDictionary = "maps"
    }
    
    static func == (lhs: Country, rhs: Country) -> Bool {
        return lhs.cca3 == rhs.cca3
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(cca3)
    }
    
}

class CountryName: Decodable {
    
    var common: String?
    var official: String?
    private var nativeName: [String: CountryNativeName]?
    
    lazy var nativeNames: [CountryNativeName] = {
        return nativeName?.compactMap({ (languageCode, name) in
            name.languageCode = languageCode
            return name
        }) ?? []
    }()
    
}

class CountryNativeName: Decodable {
    var languageCode: String?
    let official: String
    let common: String
}

class CountryCurrency: Decodable {
    var currencyCode: String?
    let name: String
    let symbol: String?
}

class CountryLanguage: Decodable {
    
    let languageCode: String
    let language: String
    
    init(languageCode: String, language: String) {
        self.languageCode = languageCode
        self.language = language
    }
    
}
