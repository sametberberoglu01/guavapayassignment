//
//  CountryListServiceClient.swift
//  GuavaPayAssignment
//
//  Created by Samet Berberoğlu on 23.05.2022.
//

import Foundation

struct CountryListServiceClient: NetworkProviderProtocol {
    
    let urlSession: SessionProtocol
    let service: NetworkService
    
    init(urlSession: SessionProtocol = Session(), clientService: NetworkService) {
        self.urlSession = urlSession
        self.service = clientService
    }
    
}
