//
//  CountryListService.swift
//  GuavaPayAssignment
//
//  Created by Samet Berberoğlu on 23.05.2022.
//

import Foundation

struct CountryListService: NetworkService {
    
    enum Endpoint {
        case continent(continent: Continent)
        case allCountries
        case countryDetail(code: String)
    }
    
    let endpoint: Endpoint
    
    var baseURL: String { "https://restcountries.com" }
    
    var path: String { "/v3.1/\(endpoint.path)" }
    
    var method: HttpMethod { .get }
    
    var httpBody: Encodable? { nil }
    
    var headers: [String : String]? { nil }
    
    var queryParameters: [URLQueryItem]? { nil }
    
    var timeout: TimeInterval? { NetworkEnvironment.requestDefaultTimeout }
    
}

private extension CountryListService.Endpoint {
    
    var path: String {
        switch self {
        case .continent(let continent):
            return "region/\(continent.rawValue)"
        case .allCountries:
            return "all"
        case .countryDetail(let code):
            return "alpha/\(code)"
        }
    }
    
}
