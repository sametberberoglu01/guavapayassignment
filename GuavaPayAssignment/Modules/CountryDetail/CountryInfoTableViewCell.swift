//
//  CountryInfoTableViewCell.swift
//  GuavaPayAssignment
//
//  Created by Samet Berberoğlu on 27.05.2022.
//

import UIKit

class CountryInfoTableViewCell: UITableViewCell {

    @IBOutlet private weak var infoLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .customDynamicColor(.cellBackground)
        infoLabel.textColor = .customDynamicColor(.bodyActiveText)
    }

    func configure(text: String, showsDisclosureIndicator: Bool) {
        accessoryType = showsDisclosureIndicator ? .disclosureIndicator : .none
        infoLabel.text = text
        selectionStyle = showsDisclosureIndicator ? .default : .none
    }
   
}
