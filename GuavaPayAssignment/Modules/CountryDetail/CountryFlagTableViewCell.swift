//
//  CountryFlagTableViewCell.swift
//  GuavaPayAssignment
//
//  Created by Samet Berberoğlu on 27.05.2022.
//

import UIKit

class CountryFlagTableViewCell: UITableViewCell {

    @IBOutlet private weak var flagImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .clear
        selectionStyle = .none
    }
    
    func configure(flagUrlString: String?) {
        guard let flagUrlString = flagUrlString,
              let flagUrl = URL(string: flagUrlString) else {
            return
        }

        ImageFetcher.shared.load(url: flagUrl, placeholderImage: UIImage()) { [weak self] imageItem in
            self?.flagImageView.image = imageItem.image
        }
        
    }
    
}
