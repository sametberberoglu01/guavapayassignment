//
//  CountryDetailViewController.swift
//  GuavaPayAssignment
//
//  Created by Samet Berberoğlu on 27.05.2022.
//

import UIKit

final class CountryDetailViewController: BaseViewController {
  
    @IBOutlet private weak var tableView: UITableView!
    
    private var country: Country?
    
    private lazy var viewModel = CountryDetailViewModel(country: country)
    
    class func create(country: Country?) -> CountryDetailViewController {
        let vc = CountryDetailViewController.sb_instantiateInstance()
        vc.country = country
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        handleStateChanges()
    }
    
    private func setupUI() {
        navigationItem.title = country?.name?.official
        tableView.sb_registerCell(CountryFlagTableViewCell.self)
        tableView.sb_registerCell(CountryInfoTableViewCell.self)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    private func handleStateChanges() {
        viewModel.addChangeHandler { [weak self] state in
            guard let self = self else { return }
            switch state {
            case .fetching:
                self.requestInProgress = true
            case .fetchingCountryDetailSucceeded:
                self.requestInProgress = false
                let detailVC = CountryDetailViewController.create(country: self.viewModel.countryDetail)
                self.navigationController?.pushViewController(detailVC, animated: true)
                self.viewModel.countryDetail = nil
            case .fetchingCountryDetailFailed(let errorMessage):
                self.requestInProgress = false
                self.presentWarningAlertController(message: errorMessage)
            }
        }
    }
    
    override class var storyboardName: String { "Countries" }
    
}

extension CountryDetailViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let section = viewModel.sections[indexPath.section]
        
        if section == .neighbor {
            viewModel.fetchCountryDetail(for: viewModel.text(for: indexPath))
        } else if section == .googleMaps {
            let vc = WebViewController(baseUrlString: country?.googleMapsUrlString, navigaitonTitle: country?.name?.official)
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let section = viewModel.sections[section]
        return section.sectionTitle
    }
    
}

extension CountryDetailViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows(in: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let section = viewModel.sections[indexPath.section]
        
        if section == .flag {
            let cell: CountryFlagTableViewCell = tableView.sb_dequeueCell(for: indexPath)
            cell.configure(flagUrlString: country?.flagUrlString)
            return cell
        }
        
        let showsDisclosureIndicator = section == .neighbor || section == .googleMaps
        let cell: CountryInfoTableViewCell = tableView.sb_dequeueCell(for: indexPath)
        cell.configure(text: viewModel.text(for: indexPath),
                       showsDisclosureIndicator: showsDisclosureIndicator)
        return cell
    }
    
}
