//
//  CountryDetailViewModel.swift
//  GuavaPayAssignment
//
//  Created by Samet Berberoğlu on 27.05.2022.
//

import Foundation
import Combine

final class CountryDetailViewModel: BaseViewModel<CountryDetailViewModel.State> {
    
    enum State: StateChange {
        case fetching
        case fetchingCountryDetailSucceeded
        case fetchingCountryDetailFailed(errorMessage: String?)
    }
    
    enum Section: Int {
        case flag = 0, currency, language, neighbor, googleMaps
    }
    
    var sections = [Section]()
    
    var countryDetail: Country?
    
    var numberOfSections: Int {
        return sections.count
    }
    
    private weak var country: Country!
    private var sectionSet = Set<Section>()
    private var cancellables = Set<AnyCancellable>()
    private var repository: CountryListRepositoryProtocol
    
    init(country: Country?, repository: CountryListRepositoryProtocol = CountryListRepository()) {
        self.country = country
        self.repository = repository
        super.init()
        setSections()
    }
    
    func fetchCountryDetail(for code: String?) {
        
        guard let code = code,
        let repository = repository as? CountryListRepository else {
            return
        }
        
        emit(change: .fetching)
        
        repository.fetchCountryDetail(for: code)
        
        repository.$countryList.sink { [weak self] countries in
            self?.handleSuccess(data: countries)
        }
        .store(in: &cancellables)
        
        repository.$error.sink { [weak self] error in
            self?.handleFailure(error: error)
        }
        .store(in: &cancellables)

    }
    
    func numberOfRows(in section: Int) -> Int {
        let section = sections[section]
        switch section {
        case .flag:
            return 1
        case .currency:
            return country.currencies.count
        case .language:
            return country.languages.count
        case .neighbor:
            return country.borders?.count ?? 0
        case .googleMaps:
            return 1
        }
    }
    
    func text(for indexPath: IndexPath) -> String {
        let section = sections[indexPath.section]
        switch section {
        case .flag: return ""
        case .currency:
            let currency = country.currencies[indexPath.row]
            return "\(currency.name) (\(currency.currencyCode ?? "")) \(currency.symbol ?? "")"
        case .language:
            let language = country.languages[indexPath.row]
            return "\(language.language) (\(language.languageCode))"
        case .neighbor:
            return "\(country.borders?[indexPath.row] ?? "")"
        case .googleMaps:
            return "See in Google Maps"
        }
    }
    
    private func setSections() {
        if country.flagUrlString != nil { sectionSet.insert(.flag) }
        if !country.currencies.isEmpty { sectionSet.insert(.currency) }
        if !country.languages.isEmpty { sectionSet.insert(.language) }
        if !(country.borders?.isEmpty ?? true) { sectionSet.insert(.neighbor) }
        if country.googleMapsUrlString != nil { sectionSet.insert(.googleMaps) }
        
        sections = Array(sectionSet).sorted(by: { $0.rawValue < $1.rawValue })
    }
    
}

extension CountryDetailViewModel.Section {
    
    var sectionTitle: String? {
        switch self {
        case .flag: return nil
        case .currency: return "Currencies"
        case .language: return "Languages"
        case .neighbor: return "Neighbors"
        case .googleMaps: return "Map"
        }
    }
    
}

private extension CountryDetailViewModel {
    
    func handleSuccess(data: [Country]) {
        if data.isEmpty || data.first == countryDetail { return }
        countryDetail = data.first
        emit(change: .fetchingCountryDetailSucceeded)
    }
    
    func handleFailure(error: Error?) {
        guard let error = error else { return }
        countryDetail = nil
        emit(change: .fetchingCountryDetailFailed(errorMessage: error.localizedDescription))
    }
    
}
