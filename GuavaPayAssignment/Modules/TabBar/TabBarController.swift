//
//  TabBarController.swift
//  GuavaPayAssignment
//
//  Created by Samet Berberoğlu on 21.05.2022.
//

import UIKit

final class TabBarController: UITabBarController {
    
    enum Tab: Int, CaseIterable {
        case continents = 0, search
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    private func setupUI() {
        viewControllers = Tab.allCases.map({ $0.navigationController })
        UITabBarItem.appearance().setTitleTextAttributes([.font: UIFont.systemFont(ofSize: 10.0, weight: .medium)], for: .normal)
        tabBar.tintColor = .customDynamicColor(.navbarTint)
        
        let appearance = UITabBarAppearance()
        appearance.configureWithOpaqueBackground()
        appearance.backgroundColor = .customDynamicColor(.navbarBackground)
        appearance.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.2)
        tabBar.standardAppearance = appearance
        if #available(iOS 15.0, *) {
            tabBar.scrollEdgeAppearance = appearance
        }
    }
    
    override class var storyboardName: String { "TabBar" }
    
}

private extension TabBarController.Tab {
    
    private var title: String {
        switch self {
        case .continents: return "Continents"
        case .search: return "Search"
        }
    }
    
    private var image: UIImage? {
        switch self {
        case .continents: return UIImage(named: "tabContinents")
        case .search: return UIImage(named: "tabSearch")
        }
    }
    
    private var selectedImage: UIImage? {
        switch self {
        case .continents: return UIImage(named: "tabContinentsSelected")
        case .search: return UIImage(named: "tabSearchSelected")
        }
    }
    
    var navigationController: SBNavigationController {
        var viewController = BaseViewController()
        switch self {
        case .continents:
            viewController = ContinentListViewController.sb_instantiateInstance()
        case .search:
            viewController = SearchCountriesViewController.sb_instantiateInstance()
        }
        
        let navC = SBNavigationController(rootViewController: viewController)
        navC.tabBarItem.title = title
        navC.tabBarItem.image = image
        navC.tabBarItem.selectedImage = selectedImage
        navC.viewControllers.first?.title = title
        return navC
    }
    
}
