//
//  SearchCountriesViewModel.swift
//  GuavaPayAssignment
//
//  Created by Samet Berberoğlu on 26.05.2022.
//

import Foundation
import Combine

final class SearchCountriesViewModel: BaseViewModel<SearchCountriesViewModel.State> {
    
    enum State: StateChange {
        case fetching
        case fetchingSucceeded
        case fetchingFailed(errorMessage: String?)
        case searched
    }
    
    var title: String {
        if datasource.isEmpty {
            return "Search"
        }
        return "Search (\(datasource.count))"
    }
    
    private(set) var datasource: [Country] = []
    private var originalDataSource: [Country] = []
    private var cancellables = Set<AnyCancellable>()
    
    let repository: CountryListRepositoryProtocol
    
    init(repository: CountryListRepositoryProtocol = CountryListRepository()) {
        self.repository = repository
    }
    
    func loadData() {
        guard let repository = repository as? CountryListRepository else { return }
        
        emit(change: .fetching)
        repository.fetchAllCountries()
        
        repository.$countryList.sink { [weak self] countries in
            self?.handleSuccess(data: countries)
        }
        .store(in: &cancellables)
        
        repository.$error.sink { [weak self] error in
            self?.handleFailure(error: error)
        }
        .store(in: &cancellables)
    }
    
    func searchCountries(for text: String?) {
        guard let text = text, !text.isEmpty else {
            datasource = originalDataSource
            emit(change: .searched)
            return
        }
        
        datasource = originalDataSource.filter({
            $0.name?.common?.contains(text) == true ||
            $0.name?.official?.contains(text) == true ||
            $0.name?.nativeNames.filter({
                $0.languageCode?.contains(text) == true ||
                $0.official.contains(text) ||
                $0.common.contains(text)
            }).isEmpty == false
        })

        emit(change: .searched)
    }
    
}

private extension SearchCountriesViewModel {
    
    func handleSuccess(data: [Country]) {
        if data.isEmpty { return }
        datasource = data
        originalDataSource = data
        emit(change: .fetchingSucceeded)
    }
    
    func handleFailure(error: Error?) {
        guard let error = error else { return }
        datasource = []
        originalDataSource = []
        emit(change: .fetchingFailed(errorMessage: error.localizedDescription))
    }
    
}
