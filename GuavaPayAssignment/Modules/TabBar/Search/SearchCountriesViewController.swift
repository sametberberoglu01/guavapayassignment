//
//  SearchCountriesViewController.swift
//  GuavaPayAssignment
//
//  Created by Samet Berberoğlu on 26.05.2022.
//

import UIKit

final class SearchCountriesViewController: BaseViewController {
    
    private typealias CountryListSnapshot = NSDiffableDataSourceSnapshot<Section, Country>
    private typealias CountryListDataSource = UITableViewDiffableDataSource<Section, Country>
    
    private enum Section {
        case main
    }
    
    @IBOutlet private weak var tableView: UITableView!
    
    private lazy var viewModel = SearchCountriesViewModel()
    
    private lazy var dataSource: CountryListDataSource = {
        let dSource = CountryListDataSource.init(tableView: tableView) { tableView, indexPath, country in
            let cell: CountryTableViewCell = tableView.sb_dequeueCell(for: indexPath)
            cell.configure(country: country)
            return cell
        }
        return dSource
    }()
    
    private lazy var searchController: UISearchController = {
        let sController = UISearchController(searchResultsController: nil)
        sController.searchResultsUpdater = self
        return sController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    private func setupUI() {
        navigationItem.title = viewModel.title
        navigationItem.searchController = searchController
        tableView.delegate = self
        tableView.sb_registerCell(CountryTableViewCell.self)
        handleStateChanges()
        viewModel.loadData()
    }
    
    private func handleStateChanges() {
        viewModel.addChangeHandler { [weak self] state in
            guard let self = self else { return }
            switch state {
            case .fetching:
                self.requestInProgress = true
            case .fetchingFailed(let errorMessage):
                self.presentWarningAlertController(message: errorMessage)
                fallthrough
            case .fetchingSucceeded, .searched:
                self.requestInProgress = false
                self.navigationItem.title = self.viewModel.title
                self.applyChanges(from: self.viewModel.datasource)
            }
        }
    }
    
    private func applyChanges(from countries: [Country]) {
        var snapshot = CountryListSnapshot()
        snapshot.appendSections([.main])
        snapshot.appendItems(countries, toSection: .main)
        dataSource.apply(snapshot, animatingDifferences: false)
    }
    
    override class var storyboardName: String { "TabBar" }
    
}

extension SearchCountriesViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let country = dataSource.itemIdentifier(for: indexPath) else { return }
        let detailVC = CountryDetailViewController.create(country: country)
        navigationController?.pushViewController(detailVC, animated: true)
    }
    
}

extension SearchCountriesViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        viewModel.searchCountries(for: searchController.searchBar.text)
    }
    
}
