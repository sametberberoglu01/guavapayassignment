//
//  ContinentListViewController.swift
//  GuavaPayAssignment
//
//  Created by Samet Berberoğlu on 21.05.2022.
//

import UIKit

final class ContinentListViewController: BaseViewController {
    
    @IBOutlet private weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    private func setupUI() {
        navigationItem.title = "Continents"
        tableView.delegate = self
        tableView.dataSource = self
        tableView.sb_registerCell(ContinentTableViewCell.self)
    }
    
    override class var storyboardName: String { "TabBar" }
    
}

extension ContinentListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let countryListViewController = CountryListViewController.create(for: Continent.allCases[indexPath.row])
        navigationController?.pushViewController(countryListViewController, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 24.0
    }
    
}

extension ContinentListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Continent.allCases.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ContinentTableViewCell = tableView.sb_dequeueCell(for: indexPath)
        cell.configure(name: Continent.allCases[indexPath.row].rawValue)
        return cell
    }
    
}
