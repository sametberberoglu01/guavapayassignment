//
//  ContinentTableViewCell.swift
//  GuavaPayAssignment
//
//  Created by Samet Berberoğlu on 22.05.2022.
//

import UIKit

final class ContinentTableViewCell: UITableViewCell {

    @IBOutlet private weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .customDynamicColor(.cellBackground)
        nameLabel.textColor = .customDynamicColor(.bodyActiveText)
    }
    
    func configure(name: String) {
        nameLabel.text = name
    }
    
}
