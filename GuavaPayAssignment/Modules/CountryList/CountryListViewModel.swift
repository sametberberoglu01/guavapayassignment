//
//  CountryListViewModel.swift
//  GuavaPayAssignment
//
//  Created by Samet Berberoğlu on 23.05.2022.
//

import Foundation
import Combine

final class CountryListViewModel: BaseViewModel<CountryListViewModel.State> {
    
    enum State: StateChange {
        case fetching
        case fetchingSucceeded
        case fetchingFailed(errorMessage: String?)
    }
    
    var title: String {
        if datasource.isEmpty {
            return continent.rawValue
        }
        return "\(continent.rawValue) (\(datasource.count))"
    }
    
    private static let countriesCache = NSCache<NSString, NSArray>()
    
    private(set) var datasource: [Country] = []
    private var cancellables = Set<AnyCancellable>()
    
    private let continent: Continent
    private let repository: CountryListRepositoryProtocol
    
    init(continent: Continent, repository: CountryListRepositoryProtocol = CountryListRepository()) {
        self.continent = continent
        self.repository = repository
    }
    
    func loadData() {
        
        if let countryList = Self.countriesCache.object(forKey: NSString(string: continent.rawValue)) as? [Country],
            !countryList.isEmpty {
            datasource = countryList
            emit(change: .fetchingSucceeded)
            return
        }
        
        guard let repository = repository as? CountryListRepository else { return }
        
        emit(change: .fetching)
        repository.fetchCountryList(for: continent)
        
        repository.$countryList.sink { [weak self] countries in
            self?.handleSuccess(data: countries)
        }
        .store(in: &cancellables)
        
        repository.$error.sink { [weak self] error in
            self?.handleFailure(error: error)
        }
        .store(in: &cancellables)
    }
    
}

private extension CountryListViewModel {
    
    func handleSuccess(data: [Country]) {
        if data.isEmpty { return }
        Self.countriesCache.setObject(NSArray(array: data), forKey: NSString(string: continent.rawValue))
        datasource = data
        emit(change: .fetchingSucceeded)
    }
    
    func handleFailure(error: Error?) {
        guard let error = error else { return }
        Self.countriesCache.setObject(NSArray(array: []), forKey: NSString(string: continent.rawValue))
        datasource = []
        emit(change: .fetchingFailed(errorMessage: error.localizedDescription))
    }
    
}
