//
//  CountryTableViewCell.swift
//  GuavaPayAssignment
//
//  Created by Samet Berberoğlu on 25.05.2022.
//

import UIKit

final class CountryTableViewCell: UITableViewCell {

    @IBOutlet private weak var flagLabel: UILabel!
    @IBOutlet private weak var latinNameLabel: UILabel!
    @IBOutlet private weak var nativeNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .customDynamicColor(.cellBackground)
        latinNameLabel.textColor = .customDynamicColor(.bodyActiveText)
        nativeNameLabel.textColor = .customDynamicColor(.bodyPassiveText)
    }

    func configure(country: Country) {
        flagLabel.text = country.flag
        latinNameLabel.text = country.name?.official
        nativeNameLabel.text = country.name?.nativeNames.first?.official
    }
    
}
