//
//  BaseViewController.swift
//  GuavaPayAssignment
//
//  Created by Samet Berberoğlu on 21.05.2022.
//

import UIKit

class BaseViewController: UIViewController {
    
    var requestInProgress: Bool = false {
        didSet {
            view.isUserInteractionEnabled = !requestInProgress
            requestInProgress ? activityIndicator.startAnimating() : activityIndicator.stopAnimating()
        }
    }
    
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView(style: .large)
        indicator.hidesWhenStopped = true
        indicator.center = view.center
        view.addSubview(indicator)
        return indicator
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .customDynamicColor(.bodyBackground)
    }
    
}
