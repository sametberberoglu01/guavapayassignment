//
//  GuavaPayAssignmentTests.swift
//  GuavaPayAssignmentTests
//
//  Created by Samet Berberoğlu on 27.05.2022.
//

import XCTest
import Combine
@testable import GuavaPayAssignment

class GuavaPayAssignmentTests: XCTestCase {
    
    private lazy var countryListRepository: CountryListRepositoryProtocol = CountryListRepository(client: self)
    private lazy var searchCountriesViewModel = SearchCountriesViewModel(repository: countryListRepository)
    private var cancellabes = Set<AnyCancellable>()
    
    private lazy var _urlSession: SessionProtocol = {
        let configuration = URLSessionConfiguration.ephemeral
        configuration.protocolClasses = [MockUrlProtocol.self]
        let session = URLSession(configuration: configuration)
        return Session(session: session)
    }()

    func testAllCountries() {
        let data = generateDataFrom(fileName: "AllCountries")
        let referenceCountries = decoded(dataType: [Country].self, from: data)
        
        let expectation = XCTestExpectation(description: "response")
        
        searchCountriesViewModel.addChangeHandler { [weak self] state in
            guard let self = self else { return }
            switch state {
            case .fetching:
                break
            case .fetchingSucceeded:
                for (index, country) in self.searchCountriesViewModel.datasource.enumerated() {
                    XCTAssertEqual(country.cca3, referenceCountries[index].cca3)
                    XCTAssertEqual(country.flag, referenceCountries[index].flag)
                }
                expectation.fulfill()
            case .fetchingFailed(_):
                break
            case .searched:
                break
            }
        }
        
        prepareForTestAndLoadData(with: data)
        wait(for: [expectation], timeout: 20.0)
    }
    
    //MARK: Helpers
    private func prepareForTestAndLoadData(with referenceData: Data) {
        MockUrlProtocol.testSamples = [service.urlRequest.url: referenceData]
        (searchCountriesViewModel.repository as? CountryListRepository)?.client = self
        searchCountriesViewModel.loadData()
    }
    
    private func generateDataFrom(fileName: String) -> Data {
        let path = Bundle(for: Self.self).path(forResource: fileName, ofType: "json")
        XCTAssertNotNil(path, "the file can not be found in the current bundle")
        let data = try? Data(contentsOf: URL(fileURLWithPath: path!), options: .mappedIfSafe)
        XCTAssertNotNil(data, "data conversion failure")
        return data!
    }
    
    private func decoded<T: Decodable>(dataType: T.Type, from data: Data) -> T {
        let decoder = JSONDecoder()
        let decodedObject = try? decoder.decode(dataType, from: data)
        XCTAssertNotNil(decodedObject, "decodingError")
        return decodedObject!
    }

}

extension GuavaPayAssignmentTests: NetworkProviderProtocol {
    
    var urlSession: SessionProtocol {
        return _urlSession
    }
    
    var service: NetworkService {
        return CountryListService(endpoint: .allCountries)
    }
    
}
